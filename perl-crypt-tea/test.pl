use Crypt::Tea qw(:ALL);

$key = 'tailored|crypted';
$rounds = 32;
#$cipher = Crypt::TEA->new($key, $rounds);

$plaintext = "some message";
#$ascii_cyphertext = $cipher->encrypt($plaintext);
$ascii_cyphertext = &encrypt($plaintext, $key);
#$plaintext_again = &decrypt ($ascii_cyphertext, $key);
#$signature = &asciidigest ($text);

#print Crypt::Tea::ascii2binary($ascii_cyphertext);
print $ascii_cyphertext;
print "\n";
