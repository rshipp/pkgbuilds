use Crypt::XTEA_PP;
use Crypt::CBC;

$key = "tailored|crypted";

my $xtea = Crypt::XTEA_PP->new( $key );
my $cbc = Crypt::CBC->new( -cipher => $xtea );

my $text = 'The quick brown fox jumps over the lazy dog.';
my $cipher_text = $cbc->encrypt( $text );

my $plain_text = $cbc->decrypt( $cipher_text );

print "$cipher_text";
print "\n";
